(function ($) {
  $(document).ready(function(){
    var optionset = Drupal.settings.slideJax.optionset;
    var slideshowSpeed = Drupal.settings.flexslider.optionsets[optionset].slideshowSpeed;
    var slides = Drupal.settings.slideJax.slides;
    var len = slides.length;
    var i = 0;
    var s = i + 1;
    // Load all slides if navigation controller clicked
    $('.flex-control-nav li a').click(function() {
      for (var i = 0; i < len; i++) {
        var selector = '.flexslider .slides li .slidejax-slide-' + s;
        var $item = $(selector);
        if ($item.text().length === 0) {
          var string = slides[i];
          $item.append(string);
        }
        s++;
      }
      clearInterval(interval);
    });
    // setInterval to slideshow speed to append markup to next slide
    var interval = setInterval(function() {
      if (i < len) {
        var selector = '.flexslider .slides li .slidejax-slide-' + s;
        var $item = $(selector);
        //console.log('interval item: ' + JSON.stringify($item));
        var string = slides[i];
        $item.append(string);
        i++;
        s++;
      } else if (i == len) {
        clearInterval(interval);
      }
    }, slideshowSpeed);
  });
})(jQuery);
